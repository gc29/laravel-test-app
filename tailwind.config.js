module.exports = {
  content: [
    './storage/framework/views/*.php',
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
    "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",
  ],
  theme: {
    container: {
        center: true,
        padding: '2rem',
        screens: {
            sm: '600px',
            md: '728px',
            lg: '984px',
        },
    },
    extend: {},
  },
  plugins: [],
}
