@extends('app')

@section('content')


<section class="job-results mt-10 mb-10">
    <div class="container mx-lg px-4">
        <h1 class="text-2xl font-bold">Jobs</h1>

        <jobs-list></jobs-list>
    </div>
</section>

@endsection
