<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="UTF-8">
  <title>Jobs</title>

  {{-- Mobile Specific Meta --}}
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <link rel="canonical" href="{{ URL::current() }}">

  <meta name="csrf-token" content="{{ csrf_token() }}">

  {{-- Fonts & Style CSS --}}
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body>
    <div id="app">
        @include('layout.header')

        @yield('content')

        @include('layout.footer')
    </div>
  <script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>
