require('./bootstrap');

import { createApp } from 'vue';
import JobsList from './components/JobsList';

const app = createApp({});
app.component('jobs-list', JobsList);
app.mount('#app');
