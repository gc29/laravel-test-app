<?php

namespace App\Console\Commands;

use App\Models\Job;
use Goutte\Client;
use Illuminate\Console\Command;

class CrawlJobData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:jobs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl dummy jobs from bestjobs.eu';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client();
        $crawler = $client->request('GET', 'https://www.bestjobs.eu/ro/locuri-de-munca-in-bucuresti/symfony');

        $crawler->filter('.job-card')->each(function ($node) {
            Job::create([
                'title' => $node->attr('data-title'),
                'company' => $node->attr('data-employer-name'),
                'location' => $node->filter('.card-footer .stretched-link-exception')->text()
            ]);
        });
    }
}
