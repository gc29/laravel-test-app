<?php

namespace App\Http\Controllers;

use App\Models\Cv;
use App\Models\Job;
use Illuminate\Http\Request;
use App\Http\Resources\JobResource;

class JobsController extends Controller
{
    /**
     * Search jobs
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function searchJobs(Request $request)
    {
        $request->validate([
            'query' => 'sometimes'
        ]);

        if($q = $request->input('query')) {
            $jobs = Job::whereRaw(
                "MATCH(title,company,location) AGAINST(?)",
                [$q]
            )->get();
        } else {
            $jobs = Job::all();
        }

        return JobResource::collection($jobs);
    }

    /**
     * Display the single job page.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function singleJob(Request $request, $slug)
    {
        $request->validate([
            'keyword' => 'sometimes'
        ]);

        $job = Job::where('slug', $slug)->firstOrFail();

        if($keyword = $request->input('keyword')) {
            $cvs = Cv::selectRaw(
                    "id, name, address, experience, MATCH(address, work, education, experience) AGAINST(?) as score",
                    array($keyword)
                )
                ->having('score', '>', 0)
                ->orderBy('score', 'desc')
                ->get();
        }

        return view('pages.single-job', [
            'job' => $job,
            'cvs' => $cvs ?? [],
        ]);
    }
}
