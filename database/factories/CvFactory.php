<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cv>
 */
class CvFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $skills = ['php, symfony', 'laravel, php', 'wordpress, php', 'react, javascript', 'vue', 'symfony'];

        return [
            'name' => $this->faker->name(),
            'address' => $this->faker->address(),
            'education' => $this->faker->text(),
            'work' => $this->faker->text(),
            'experience' => $skills[rand(0,5)]
        ];
    }
}
